﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace indirimhaber.Controllers
{
    public class HaberController : Controller
    {
        
        FırsatHaberleriEntities db = new FırsatHaberleriEntities();
        public ActionResult Index()
        {
            ViewBag.Kategoriler = new SelectList(db.Kategori.ToList(),"ID","KategoriAd");
            var model = db.Haber.ToList();
            return View(model);


        }
        
        
            
        [HttpPost]    
        public ActionResult Index(Models.HaberAramaModel arama)
        {
            //Arama Yaparken kullanılıyor.
            //Textbox içindekiyle HaberBasliklar karşılaştırılır.
            //Armaa Çalışmıyor.
            var sorgu = db.Haber.AsQueryable();
            if(!string.IsNullOrEmpty(arama.HaberBaslik))
            {
                sorgu = sorgu.Where(x => x.HaberBaslik.Contains(arama.HaberBaslik));
            }
            if(arama.Kategoriler>0)
            {
                sorgu = sorgu.Where(x => x.FKKategoriID == arama.Kategoriler);
            }
            //Kategoriler DropdownList Doldurur.
            ViewBag.Kategoriler = new SelectList(db.Kategori.ToList(), "ID", "KategoriAd");
            var model = sorgu.ToList();
            return View(model);
        }

        public ActionResult Detay(int id)
        {
            var model = db.Haber.Find(id);
            return View(model);
        }

        public ActionResult Olustur()
        {
            ViewBag.FKKategoriID = new SelectList(db.Kategori.ToList(), "ID", "KategoriAd");
            return View();
        }

        
        [HttpPost]
        public ActionResult Olustur(Haber model)
        {
            
            //   model.EklenmeTarihi = DateTime.Now();   Eklenme Tarihi Otomatik olarak atanır.
            if (ModelState.IsValid)
            {
                db.Haber.Add(model);
                db.SaveChanges();

            }
            return RedirectToAction("Index");
        }
        
        //Haber/Duzenle/1 
        
        public ActionResult Duzenle(int id)
        {
            ViewBag.FKKategoriID = new SelectList(db.Kategori.ToList(), "ID", "KategoriAd");

            var model = db.Haber.Find(id);
            return View(model);
        }


        [HttpPost]
        public ActionResult Duzenle(Haber model)
        {

            if(ModelState.IsValid)
            {
                var kayit = db.Haber.Find(model.ID);
                TryUpdateModel(kayit);
                db.SaveChanges();
                ViewBag.KayitBasarili = true;
            }
            else
            {
                ViewBag.KayitBasarili = false;
            }
            ViewBag.FKKategoriID = new SelectList(db.Kategori.ToList(), "ID", "KategoriAd");
            return View();
        }
        
        public ActionResult Sil(int id)
        {
            var model = db.Haber.Find(id);
            return View();
        }
        
        [HttpPost]
        public ActionResult SilOnay(int id)
        {

            if(ModelState.IsValid)
            {
                var model = db.Haber.Find(id);
                db.Haber.Remove(model);
                db.SaveChanges();

            }

            return RedirectToAction("Index");
        }
    }
}
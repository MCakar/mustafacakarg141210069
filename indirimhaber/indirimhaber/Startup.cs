﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(indirimhaber.Startup))]
namespace indirimhaber
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
